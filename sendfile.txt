Microsoft Windows [Version 6.3.9600]
(c) 2013 Microsoft Corporation. All rights reserved.

C:\Users\Aditi Bhalerao>cd desktop

C:\Users\Aditi Bhalerao\Desktop>sh

Aditi Bhalerao@Aditi MINGW64 ~/Desktop
$ git clone https://gitlab.com/archanabodke123/demo-project.git
Cloning into 'demo-project'...
remote: Enumerating objects: 14, done.
remote: Counting objects: 100% (14/14), done.
remote: Compressing objects: 100% (13/13), done.
remote: Total 14 (delta 2), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (14/14), 427.67 KiB | 262.00 KiB/s, done.
Resolving deltas: 100% (2/2), done.

Aditi Bhalerao@Aditi MINGW64 ~/Desktop
$ cd demo-project/

Aditi Bhalerao@Aditi MINGW64 ~/Desktop/demo-project (master)
$ git checkout test_branch
Switched to a new branch 'test_branch'
Branch 'test_branch' set up to track remote branch 'test_branch' from 'origin'.

Aditi Bhalerao@Aditi MINGW64 ~/Desktop/demo-project (test_branch)
$ touch mergereq.txt

Aditi Bhalerao@Aditi MINGW64 ~/Desktop/demo-project (test_branch)
$ notepad mergereq.txt

Aditi Bhalerao@Aditi MINGW64 ~/Desktop/demo-project (test_branch)
$ git add mergereq.txt

Aditi Bhalerao@Aditi MINGW64 ~/Desktop/demo-project (test_branch)
$ git commit -m "added Merge file"
[test_branch cd46b15] added Merge file
 1 file changed, 144 insertions(+)
 create mode 100644 mergereq.txt

Aditi Bhalerao@Aditi MINGW64 ~/Desktop/demo-project (test_branch)
$ git push -u origin test_branch
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 4 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 1.40 KiB | 286.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for test_branch, visit:
remote:   https://gitlab.com/archanabodke123/demo-project/-/merge_requests/new?merge_request%5Bsource_branch%5D=test_branch
remote:
To https://gitlab.com/archanabodke123/demo-project.git
   ff11bcc..cd46b15  test_branch -> test_branch
Branch 'test_branch' set up to track remote branch 'test_branch' from 'origin'.

